package com.jingpeng.topo;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Create by IntelliJ IDEA
 *
 * @Author chenlei
 * @DateTime 2018/7/11 17:00
 * @Description DigestHttpClientUtil
 */
public class DigestHttpClientUtil {

    public String  main(String url) throws IOException{
//    	创建httpclient
        HttpClient digestHttpClient = digestHttpClient("admin","123456",null,null);
//      设置请求超时时间
        RequestConfig requestConfig = RequestConfig.custom()  
                .setConnectTimeout(3000).setConnectionRequestTimeout(1000)  
                .setSocketTimeout(3000).build();
//        创建请求
        HttpGet httpGet = new HttpGet(url);
        
        httpGet.setConfig(requestConfig);  
//        try {
//            System.out.println("=============digest auth=====================");
//            System.out.println(EntityUtils.toString(digestHttpClient.execute(httpGet).getEntity()));
//            System.out.println();
//        } catch (Exception e) {
//        }
        try {
//        	获取状态码
        	int result = digestHttpClient.execute(httpGet).getStatusLine().getStatusCode();
        	if(result==200) {
//        		返回实体
            	return EntityUtils.toString(digestHttpClient.execute(httpGet).getEntity());
        	}else {
				return re+url;
			}
        } catch (Exception e) {
			return re+url;
		}
    }

    public static HttpClient digestHttpClient(@NotNull String username, @NotNull String password, @Nullable String host, @Nullable Integer port){
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(new AuthScope(isBlank(host) ? AuthScope.ANY_HOST : host, port == null ? AuthScope.ANY_PORT : port),
                new UsernamePasswordCredentials(username,password));
        return HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
    }

    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for(int i = 0; i < strLen; ++i) {
                // 判断字符是否为空格、制表符、tab
                if (!Character.isWhitespace(str.charAt(i))) {    
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }

}
    String re = "ipaddress:";
}



