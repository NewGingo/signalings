package com.jingpeng.topo;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONObject;

public class TopoData {
	private HttpServletRequest request = ServletActionContext.getRequest();
	private JSONObject json;



	public String main() throws IOException {
		DigestHttpClientUtil http =new DigestHttpClientUtil();
		String url = request.getParameter("url");
		System.out.println("请求地址:"+url);
		json = JSONObject.fromObject(http.main(url));
		return "success";
	}
	public String jsonData() throws Exception{
		System.out.println("发送json");
		Hello hello = new Hello();
		json = JSONObject.fromObject(Hello.main());
		return "success";
	}
	public String addJson() throws Exception{
		String data = request.getParameter("data");
//		System.out.println("son"+son);
		Hello hello = new Hello();
		json = JSONObject.fromObject(hello.add(data));
		System.out.println(json);
		return "success";
	}
	
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}
}
