        /**
         *基于D3.js拓扑图
         *
         */

        "use strict";

        ///Width and height
        var w = $("#topology_container").width();
        var h = $("#topology_container").height();
        var isFirstLoad = true,
            dataset, alarms;
        //Original data
        var dataset = {
            nodes: [],
            paths: []
        };

        function Topology() {
            this.config = {
                lineColorMap: {
                    /*上级平台*/
                    down: 'red',
                    /*互联平台*/
                    normal: 'green',
                    /*下级平台*/
                    high: 'yellow'
                }
            };
            this.tipInfo = {
                type: '', //line or node
                data: {}
            }
        }

        Topology.prototype.draw = function() {
            var self = this;
            /*$.ajax({
                url:'http://10.138.77.242:8360/topology/to_json/',
                method: 'get'
            }).done(function(data){
                dataset = data;
                //self.getAlarm();//获取告警

            })*/
            self.initForce(); //初始布局
            self.createElem(); //创建元素
            Topology.bindEvent.apply(self); //事件绑定
        };

        Topology.prototype.initForce = function() {
            var self = this;
            //Initialize a default force layout, using the nodes and edges in dataset
            self.force = d3.layout.force()
                .nodes(dataset.nodes)
                .links(dataset.paths)
                .size([w, h])
                .linkDistance(function() {
                    return 300;
                })
                .charge([-100])
                .on("tick", tick)
                .on("end", tickEnd)
                .start();

            this.drag = this.force.drag()
                .on("dragstart", function(d) {
                    d3.event.sourceEvent.stopPropagation();
                })
                .on("drag", function() {
                    //self.stopTick(5);
                });

            function tick() {
                self.link.selectAll("line")
                    .each(function(d) {
                        var elem = d3.select(this),
                            offset = self.getOffset(d, elem, 6);
                        elem
                            .attr("x1", function(d, i) {
                                return d.source.x + offset.offsetX;
                            })
                            .attr("y1", function(d, i) {
                                return d.source.y + offset.offsetY;
                            })
                            .attr("x2", function(d, i) {
                                return d.target.x + offset.offsetX;
                            })
                            .attr("y2", function(d, i) {
                                return d.target.y + offset.offsetY;
                            });
                    })

                self.node
                    .attr("transform", function(d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    });
            }

            function tickEnd() {
                if (isFirstLoad) {
                    isFirstLoad = false;
                    self.refresh(self);
                }
            }
        }

        Topology.prototype.createElem = function() {
            var self = this;
            //Create SVG element
            var svg = d3.select("#topology_container")
                .append("svg")
                .attr("id", "network-topology")
                .attr("width", "100%")
                .attr("height", "100%")
                .call(this.zoom);

            var container = svg.append("g")
                .attr("class", "container");
            self.linkContainer = container.append("g")
                .attr("id", "line_container");

            self.nodeContainer = container.append("g")
                .attr("id", "node_container");


            self.link = self.linkContainer.selectAll(".link")
                .data(dataset.paths)
                .enter()
                .append("g")
                .attr("class", "link")
                .each(function(d) {
                    self.addLines(d3.select(this), d);
                });

            self.node = self.nodeContainer.selectAll(".node")
                .data(dataset.nodes)
                .enter()
                .append("g")
                .attr("class", "node")
                .call(this.drag);

            self.addNodeChildren(self.node);
        }

        Topology.prototype.closeAlarm = function() {
            $("#alarm").css("display", "none");
        }

        Topology.prototype.getAlarm = function() {
            $.ajax({
                url: 'http://10.138.77.242:8360/topology/alarms/',
                method: 'get'
            }).done(function(data) {
                alarms = data.alarms;

                var alarmUl = $("#alarm").find("ul"),
                    liList = '';

                $.each(alarms, function(index, value) {
                    liList = liList + '<li>' + value + '</li>';
                })
                alarmUl.html(liList);
            })
        }

        Topology.prototype.zoom = d3.behavior.zoom()
            .scaleExtent([0.1, 10])
            .on("zoom", function() {
                d3.select(".container")
                    .attr("transform", "translate(" + d3.event.translate + ")" +
                        "scale(" + d3.event.scale + ")");
            });

        Topology.prototype.getLinePos = function(index) {
            var direction, num;
            if (index % 2 == 1) {
                direction = 'up';
            } else {
                direction = 'down';
            }
            num = Math.floor((index + 1) / 2);
            return direction + "_" + num;
        };

        //计算线相对于中心线的偏移量(x坐标偏移量和y坐标偏移量)
        Topology.prototype.getOffset = function(d, elem, space) {
            var linePos = elem.attr("data-pos"),
                x1 = d.source.x,
                y1 = d.source.y,
                x2 = d.target.x,
                y2 = d.target.y,
                x, y, posArr, stepOffsetX, stepOffsetY, offsetX, offsetY;

            x = Math.abs(x2 - x1);
            y = Math.abs(y2 - y1);
            stepOffsetX = Math.ceil(space * y / Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
            stepOffsetY = Math.ceil(space * x / Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));

            posArr = linePos.split("_");
            offsetX = parseInt(posArr[1]) * stepOffsetX;
            offsetY = parseInt(posArr[1]) * stepOffsetY;

            if ((x2 - x1) * (y2 - y1) > 0) {
                if (posArr[0] == 'up') {
                    offsetX = -offsetX;
                } else {
                    offsetY = -offsetY;
                }
            } else {
                if (posArr[0] == 'up') {
                    offsetX = -offsetX;
                    offsetY = -offsetY;
                }
            }

            return {
                offsetX: offsetX,
                offsetY: offsetY
            };
        };

        //eventpos为鼠标在浏览器页面的位置,offset为鼠标位置到弹框的距离
        Topology.prototype.getTipPos = function(eventpos, offset) {
            var docScrollTop = $(document).scrollTop(),
                docScrollLeft = $(document).scrollLeft(),

                visibleHeight = $(window).height(),
                visibleWidth = $(window).width(),

                tipWidth = $("#show-tips").width(),
                tipHeight = $("#show-tips").height(),

                left = eventpos.x - docScrollLeft + offset,
                top = eventpos.y - docScrollTop - offset;

            if (eventpos.x - docScrollLeft + tipWidth + offset >= visibleWidth) {
                left = eventpos.x - docScrollLeft - tipWidth - offset;
            }

            if (eventpos.y - docScrollTop + tipHeight - offset >= visibleHeight) {
                top = eventpos.y - docScrollTop - tipHeight + offset;
            }

            return {
                "left": left,
                "top": top
            };
        }

        //展示提示框

        Topology.prototype.setTipInfo = function(type, data) {
            var self = this;

            if (data == undefined) {
                data = type;
                type = '';
            }

            if (type) {
                self.tipInfo.type = type;
            }

            self.tipInfo.data = data;

        }

        //给路径添加一条或多条线
        Topology.prototype.addLines = function(pathG, d) {
            var position, line;

            for (var i = 0; i < d.lines.length; i++) {
                position = this.getLinePos(i);

                line = pathG.append("line")
                    .attr("stroke", this.config.lineColorMap[d.lines[i].traffic_status])
                    .attr("data-pos", position + '_' + i);
            }
        };

        //节点添加内容
        Topology.prototype.addNodeChildren = function(node) {
            var self = this;
            node.append("image")
                .attr("class", "node-icon")
                .attr("transform", "translate(" + -26 + "," + -26 + ")")
                .each(function(d) {
                    self.setImage(d3.select(this), d.type);
                });

            node.append("text")
                .each(function(d) {
                    if (d.type == "collection") {
                        d3.select(this).attr("transform", "translate(" + -20 + "," + 42 + ")");
                    } else {
                        d3.select(this).attr("transform", "translate(" + -20 + "," + 34 + ")");
                    }
                })
                .text(function(d) {
                    return d.name;
                })
        }

        //添加节点图标
        Topology.prototype.setImage = function(elem, type) {
            switch (type) {
                case "router":
                    elem
                        .attr("xlink:href", "./img/computer.svg")
                        .attr("width", 55)
                        .attr("height", 55);
                    break;
                case "switchDevice":
                    elem
                        .attr("xlink:href", "./img/computer.svg")
                        .attr("width", 45)
                        .attr("height", 45);
                    break;
                case "collection":
                    elem
                        .attr("xlink:href", "./img/computer.svg")
                        .attr("width", 55)
                        .attr("height", 55);
            }
        };


        Topology.prototype.stopTick = function(n) {
            for (var i = 0; i < n; ++i) {
                this.force.tick();
            }
            this.force.stop();
        };

        //编辑信息
        Topology.prototype.editTipInfo = function(jqElem) {
            var text = jqElem.val(),
                label = jqElem.prev().prev().text(),
                label = label.substring(0, label.length - 1),
                orgParam = {};

            orgParam[label] = text;

            orgParam.type = self.tipInfo.type;

            for (var key in self.tipInfo.data) {
                if (key != label) {
                    if (key == "ip" || key == "hostname") {
                        orgParam[key] = self.tipInfo.data[key];
                    }
                }
            }

            $.ajax({
                url: '//',
                method: 'POST',
                data: JSON.stringify(orgParam)
            }).done(function() {

            })

            console.log(orgParam)

        }

        //更新拓扑图数据
        Topology.prototype.refresh = function(self) {
            var defer = $.Deferred();

            setInterval(function() {
                //toRefresh();
            }, 60000);

            function toRefresh() {
                var promise = updateData();
                promise.done(updateTopo);
            }

            function updateData() {
                $.ajax({
                    url: 'http://172.31.92.203:8080/lua/platform',
                    method: 'get'
                }).done(function(data) {
                    dataset = data;
                    self.force.links(dataset.paths)
                    defer.resolve();
                });

                return defer.promise();
            }

            function updateTopo() {
                //self.getAlarm();
                self.force
                    .links(dataset.paths)
                    .nodes(dataset.nodes);

                self.link.remove();

                self.link = self.linkContainer.selectAll(".link")
                    .data(dataset.paths)
                    .enter()
                    .append("g")
                    .attr("class", "link")
                    .each(function(d) {
                        self.addLines(d3.select(this), d);
                    });


                self.node.remove();
                self.node = self.nodeContainer.selectAll(".node")
                    .data(dataset.nodes)
                    .enter()
                    .append("g")
                    .attr("class", "node")
                    .call(self.drag);

                self.addNodeChildren(self.node);

                self.force.start();
            }
        }

        //事件绑定
        Topology.bindEvent = function() {
            var selfColor, elem, lineData, linePos, nodeInfo
            self = this;

            //处理input没有focus情况
            function hideInput() {
                $("#show-tips").find("input").each(function() {
                    if ($(this).css("display") != "none") {
                        $(this).css("display", "none").prev().css("display", "inline-block");
                    }
                })
            }

            d3.select("#line_container")
                .on("click", function() {
                    d3.event.stopPropagation();
                    linePos = elem.attr("data-pos").split('_')[2];

                    //相对于浏览器页面的坐标
                    var point = {
                        x: d3.event.pageX,
                        y: d3.event.pageY
                    }

                    self.setTipInfo("line", elem.datum().lines[linePos]);

                    self.toShowTip(point);
                })
                .on("mouseover", function() {
                    elem = d3.select(d3.event.target);

                    selfColor = elem.attr("stroke");
                    elem.attr("stroke", "white");

                })
                .on("mouseout", function() {
                    elem.attr("stroke", selfColor);
                });

            d3.select("#node_container")
                .on("click", function() {
                    if (d3.event.defaultPrevented) return;
                    d3.event.stopPropagation();

                    var point = {
                        x: d3.event.pageX,
                        y: d3.event.pageY
                    }

                    self.setTipInfo("node", d3.select(d3.event.target).datum().info);

                    self.toShowTip(point);
                });

            $("#network-topology").click(function() {
                Topology.hideTip();
            })


            $("#alarm")
                .on("click", ".close", function() {
                    self.closeAlarm();
                })
                .on("click", "#status", function() {
                    var statusElem = $(this);

                    if (statusElem.html().indexOf("收起告警") != -1) {
                        $("#alarm #content").slideUp(function() {
                            statusElem.html("展开告警&nbsp;");
                            $("#alarm").animate({
                                width: '100px'
                            });
                        });
                    } else {
                        $("#alarm").animate({
                            width: '500px'
                        }, function() {
                            $("#alarm #content").slideDown(function() {
                                statusElem.html("收起告警&nbsp;");
                            });
                        });
                    }
                });

            $("#show-tips").on("click", ".close", function() {
                Topology.hideTip();
            }).on("click", "i", function(e) {
                e.stopPropagation();

                hideInput();
                var text = $(this).closest('span').text();
                $(this).closest('span').css("display", "none").next().css("display", "inline-block").val(text);

            }).on("blur", "input", function() {
                //update function
                //get data
                self.editTipInfo($(this));
                $(this).css("display", "none").prev().css("display", "inline-block");
            }).on("click", "input", function(e) {
                e.stopPropagation();
            }).on("click", function() {
                hideInput();
            });

            function random(lower, upper) {
                return Math.floor(Math.random() * (upper - lower + 1)) + lower;
            }
            var http = "http://";
            var config = ":8080/lua/system/config";
            var platform = ":8080/lua/platform";

            function myajax(url) {
                var str
                $.ajax({
                    type: "post",
                    url: "TopoAction.action",
                    async: false,
                    data: {
                        url: http + url + config
                    },
                    success: function(data) {
                        if (data.payload != null) {
                            str = data.payload;
                        } else {
                            str = data;
                        }
                    },
                    error: function() {
                        str = {
                            ipaddress: url
                        };
                    }
                });
                return str;
            };

            function myajax2(url) {
                var st
                $.ajax({
                    type: "post",
                    url: "TopoAction.action",
                    async: false,
                    data: {
                        url: http + url + platform
                    },
                    success: function(data) {
                        st = data;
                    },
                    error: function() {
                        st = {
                            ipaddress: url
                        };
                    }
                });
                return st;
            };
            $("#syn").click(function() {
                var url = "172.31.92.203";
                var array = new Array();
                var sta = false;
                array[0] = myajax(url);
                $.extend(array[0], myajax2(url));
                for (let i = 0; i < array.length; i++) {
                    if (array[i].payload != null) {
                        for (let q = 0; q < array[i].payload.length; q++) {
                            url = array[i].payload[q].F_IPADDRESS;
                            sta = false;
                            for (let a = 0; a < array.length; a++) {
                                if (array[a] == null) {
                                    break;
                                }
                                if (array[a].ipaddress == url) {
                                    sta = true;
                                    break;
                                }
                            };
                            // 已存在则直接跳过
                            if (sta == true) {
                                continue;
                            } else if (sta == false) {
                                array.push(myajax(url));
                            }
                            if (array[array.length - 1].ipaddress != null) {
                                $.extend(array[array.length - 1], myajax2(array[array.length - 1].ipaddress));
                            }

                        };
                    }
                };
                for (let z = 0; z < array.length; z++) {
                    $.extend(array[z], {
                        id: z
                    });
                    if (array[z].ipaddress != null) {
                        dataset.nodes.push({
                            node_id: z,
                            type: "router",
                            name: array[z].ipaddress,
                            start: "ok",
                            x: 338 + random(1, 700),
                            y: 55 + random(1, 300),
                            fixed: true,
                            info: {
                                name: array[z].ipaddress
                            }
                        });
                    };
                    if (array[z].ipaddress == null) {
                        dataset.nodes.push({
                            node_id: z,
                            type: "router",
                            name: JSON.stringify(array[z]),
                            start: "ok",
                            x: 338 + random(1, 700),
                            y: 55 + random(1, 300),
                            fixed: true,
                            info: {
                                name: array[z].ipaddress
                            }
                        });
                    }
                };
                for (let i = 0; i < array.length; i++) {
                    if (array[i].payload != null) {

                        for (let q = 0; q < array[i].payload.length; q++) {
                            for (let a = 0; a < array.length; a++) {
                                if (array[a].ipaddress == array[i].payload[q].F_IPADDRESS) {
                                    switch (array[i].payload[q].F_CATEGORY) {
                                        case "PLAT_PARENT":
                                            {
                                                dataset.paths.push({
                                                    source: array[i].id,
                                                    target: array[a].id,
                                                    lines: [{
                                                        traffic_status: "down",
                                                        category: "上级平台" /*红色*/
                                                    }]
                                                });
                                                break;
                                            }
                                        case "PLAT_SIBLING":
                                            {
                                                dataset.paths.push({
                                                    source: array[i].id,
                                                    target: array[a].id,
                                                    lines: [{
                                                        traffic_status: "normal",
                                                        category: "互联平台" /*绿色*/
                                                    }]
                                                });
                                                break;
                                            }
                                        case "PLAT_CHILD":
                                            {
                                                dataset.paths.push({
                                                    source: array[i].id,
                                                    target: array[a].id,
                                                    lines: [{
                                                        traffic_status: "high",
                                                        category: "下级平台" /*黄色*/
                                                    }]
                                                });
                                                break;
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
                console.log(array);
                $("svg").remove();
                topology.draw();
                console.log(dataset);
            });
        }

        //展示提示框,工具函数
        Topology.showTip = function() {
                var display = $("#show-tips").css("display");

                if (display == "none") {
                    $("#show-tips").css("display", "block");
                }
            }
            //隐藏提示框且提示框内容置空
        Topology.hideTip = function() {
            var display = $("#show-tips").css("display");

            if (display == "block") {
                $("#show-tips")
                    .css("display", "none")
                    .find("ul")
                    .empty();
            }
        }

        function TopoApiService() {
            $.ajaxSetup({
                dataType: 'json',
                contentType: 'application/json;charset=utf-8',
                statusCode: {
                    400: function(e) {
                        console.log(e.responseJSON.errmsg);
                    },
                    404: function() {
                        console.log("请求资源不存在!");
                    }
                }
            })

            return {
                gettopo: function() {
                    return $.ajax({
                        url: '/topo/gettopo',
                        method: 'POST'
                    })
                },
                updatetopo: function(data) {
                    return $.ajax({
                        url: '/topo/updatetopo',
                        method: 'POST',
                        data: JSON.stringify(data)
                    })
                }
            }
        }

        var topology = new Topology();
        topology.draw();