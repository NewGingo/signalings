import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Topo from '@/components/Topo'
import Tree from '@/components/Tree'
import Welcome from '@/components/Welcome'
import Table from '@/components/Table'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    redirect: 'Login'
  },
  {
    path: '/Home',
    name: 'Home',
    component: Home,
    children: [{
      path: '/Topo',
      component: Topo
    },
    {
      path: '/Tree',
      component: Tree
    },
    {
      path: '/Welcome',
      component: Welcome
    },
    {
      path: '/Table',
      component: Table
    }
    ]
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  }
  ]
})
